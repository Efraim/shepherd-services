;; This channel is needed for BNW at a minimum. Later commits include a
;; 'core-update' merge which breaks some of the dependencies.

(list
  (channel
    (name 'gn-bioinformatics)
    (url "https://gitlab.com/genenetwork/guix-bioinformatics")
    (commit "5390e28c3308d0f9ce7ee2b96c9c4f31e3a7861b"))
  (channel
    (name 'guix)
    (url "https://git.savannah.gnu.org/git/guix.git")
    (commit "8a0b02c248a9877812cce29641bb1843cb77ea19")))

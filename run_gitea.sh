#!/bin/sh
# This version for the gitea guix profile which includes guix-bioinformatics, using gitea-channels.scm.
/bin/su -l gitea -c 'GITEA_WORK_DIR=/export/git/gitea HOME=/export/git/gitea /home/shepherd/guix-profiles/gitea/bin/guix environment --ad-hoc gitea -- gitea --port 3300'

#/bin/su -l gitea -c 'GITEA_WORK_DIR=/export/git/gitea HOME=/export/git/gitea $(/home/shepherd/guix-profiles/gitea/bin/guix build -L /home/shepherd/guix-bioinformatics gitea)/bin/gitea --port 3300'

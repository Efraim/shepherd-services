(define ratspub
  (make <service>
	#:provides '(ratspub)
	#:docstring "Run the rats.pub web server"
	#:start (make-forkexec-constructor
		  ;(system* "/var/guix/profiles/per-user/shepherd/current-guix/bin/guix" "environment" "--ad-hoc" "ratspub" "--" "./server.py")
		  '("/home/shepherd/run_ratspub.sh")
		  ;#:directory (system* "/var/guix/profiles/per-user/shepherd/current-guix/bin/guix" "build" "ratspub") ; returns 0
		  #:environment-variables
		  (list "EDIRECT_PUBMED_MASTER=/export2/PubMed"
			"NLTK_DATA=/home/hchen/nltk_data"
			"PERL_LWP_SSL_CA_FILE=/etc/ssl/certs/ca-certificates.crt"
			"TMPDIR=/export/ratspub/tmp")
		  #:log-file "/home/shepherd/logs/ratspub.log")
	#:stop (make-kill-destructor)
	#:respawn? #t))
(register-services ratspub)

(start ratspub)

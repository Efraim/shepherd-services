(define ipfs
  (make <service>
	#:provides '(ipfs)
	#:docstring "Run the IPFS daemon"
	#:start (make-forkexec-constructor
		  '("/usr/bin/sudo" "/home/shepherd/run_ipfs.sh")
		  #:log-file "/home/shepherd/logs/ipfs.log")
	#:stop (make-kill-destructor)
	#:respawn? #t))
(register-services ipfs)

(start ipfs)

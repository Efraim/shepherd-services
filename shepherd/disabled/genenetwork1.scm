(define genenetwork1
  (make <service>
	#:provides '(genenetwork1)
	#:docstring "Run the Legacy Genenetwork webserver"
	#:start (make-forkexec-constructor
		  '("/usr/bin/sudo" "/home/shepherd/run_genenetwork1.sh")
		  #:log-file "/home/shepherd/logs/genenetwork1.log")
	#:stop (make-kill-destructor)
	#:respawn? #t))
(register-services genenetwork1)

(start genenetwork1)

(define test-upgrade
  (make <service>
	#:provides '(test-upgrade)
	#:docstring "Build all the services to make sure they work on an upgrade"
	#:start (make-forkexec-constructor
		  ;; First attempt to build all the packages and only afterward attempt to build the containers.
		  ;(when (zero? (system*
		  '("guix" "build"
		    "-L" "/home/shepherd/guix-bioinformatics"
		    "-L" "/home/shepherd/guix-past/modules"
		    "genenetwork1" "httpd22-with-mod-python"
		    ;"bnw" octave-3.4.3 FTBFS
		    "gitea"
		    "go-ipfs"
		    "edirect"
		    "bxd-power-calculator-app"
		    "rn6-assembly-error-app"
		    "singlecellrshiny"
		    "virtuoso-ose"
		    "hrdp-project"
		    "bh20-seq-resource")
		    ;(zero? (system* "guix" "system" "-L" "/home/shepherd/guix-bioinformatics"
		    ;		    "container" "/home/shepherd/guix-bioinformatics/gn/services/bnw-container.scm"
		    ;		    "--share=/home/shepherd/logs/bnw-server=/var/log" "--network")))
		  #:log-file "/home/shepherd/logs/test-upgrade.log")
	#:one-shot? #t))
(register-services test-upgrade)

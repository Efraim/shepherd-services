(define gitea
  (make <service>
	#:provides '(gitea)
	#:docstring "Run a Gitea instance"
	#:start (make-forkexec-constructor
		  '("/usr/bin/sudo" "/home/shepherd/run_gitea.sh")
		  #:log-file "/home/shepherd/logs/gitea.log")
	#:stop (make-kill-destructor)
	#:respawn? #t))
(register-services gitea)

(start gitea)

(define covid19-pubseq
  (make <service>
	#:provides '(covid19-pubseq)
	#:docstring "Run the COVID-19 PubSeq: Public SARS-CoV-2 Sequence Resource Webserver"
	#:start (make-forkexec-constructor
		  '("/usr/bin/sudo" "/home/shepherd/run_covid19-pubseq.sh")
		  #:log-file "/home/shepherd/logs/covid19-pubseq.log")
	#:stop (make-kill-destructor)
	#:respawn? #t))
(register-services covid19-pubseq)

(start covid19-pubseq)

(define mcron
  (make <service>
	#:provides '(mcron)
	#:docstring "Run the mcron daemon"
	#:start (make-forkexec-constructor
		  '("/var/guix/profiles/per-user/shepherd/guix-profile/bin/mcron")
		  #:log-file "/home/shepherd/logs/mcron.log")
	#:stop (make-kill-destructor)
	#:respawn? #t))
(register-services mcron)

(start mcron)

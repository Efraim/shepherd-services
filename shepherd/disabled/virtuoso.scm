(define virtuoso
  (make <service>
	#:provides '(virtuoso)
	#:docstring "Run the virtuoso web server"
	#:start (make-forkexec-constructor
		  '("/var/guix/profiles/per-user/shepherd/current-guix/bin/guix" "environment" "--ad-hoc" "virtuoso-ose" "--" "virtuoso-t" "-f")
		  #:directory "/export/virtuoso/var/lib/virtuoso/db"
		  #:log-file "/home/shepherd/logs/virtuoso.log")
	#:stop (make-kill-destructor)
	#:respawn? #t))
(register-services virtuoso)

(start virtuoso)

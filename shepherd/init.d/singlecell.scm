(define singlecell
  (make <service>
	#:provides '(singlecell)
	#:docstring "Run Single Cell R-Shiny app"
	#:start (make-forkexec-constructor
		  ;(system* "/var/guix/profiles/per-user/shepherd/current-guix/bin/guix" "environment" "--ad-hoc" "singlecellrshiny" "--" "sh" "-c" "\\'R_LIBS_USER=$GUIX_ENVIRONMENT/site-library/ singlecellrshiny\\'")
		  '("/home/shepherd/run_singlecell.sh")
		  #:log-file "/home/shepherd/logs/singlecell.log")
	#:stop (make-kill-destructor)
	#:respawn? #t))
(register-services singlecell)

(start singlecell)

(define genecup
  (make <service>
	#:provides '(genecup)
	#:docstring "Run the genecup.org web server"
	#:start (make-forkexec-constructor
		  '("/home/shepherd/run_genecup.sh")
		  #:environment-variables
		  (list "EDIRECT_PUBMED_MASTER=/export2/PubMed"
			"NLTK_DATA=/home/hchen/nltk_data"
			"PERL_LWP_SSL_CA_FILE=/etc/ssl/certs/ca-certificates.crt"
			"TMPDIR=/export/ratspub/tmp")
		  #:log-file "/home/shepherd/logs/genecup.log")
	#:stop (make-kill-destructor)
	#:respawn? #t))
(register-services genecup)

(start genecup)

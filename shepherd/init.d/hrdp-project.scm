(define hrdp-project
  (make <service>
	#:provides '(hrdp-project)
	#:docstring "Run the hrdp-project web server"
	#:start (make-forkexec-constructor
		  '("/home/shepherd/run_hrdp-project.sh")
		  #:log-file "/home/shepherd/logs/hrdp-project.log")
	#:stop (make-kill-destructor)
	#:respawn? #t))
(register-services hrdp-project)

(start hrdp-project)

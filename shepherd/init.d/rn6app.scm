(define rn6app
  (make <service>
	#:provides '(rn6app)
	#:docstring "Run RN6 Assembly Error app"
	#:start (make-forkexec-constructor
		  ;(system* "/var/guix/profiles/per-user/shepherd/current-guix/bin/guix" "environment" "--ad-hoc" "rn6-assembly-error-app" "--" "sh" "-c" "\\'R_LIBS_USER=$GUIX_ENVIRONMENT/site-library/ rn6-assembly-error-app\\'")
		  '("/home/shepherd/run_rn6app.sh")
		  #:log-file "/home/shepherd/logs/rn6app.log")
	#:stop (make-kill-destructor)
	#:respawn? #t))
(register-services rn6app)

(start rn6app)

(define genome-browser
  (make <service>
	#:provides '(genome-browser)
	#:docstring "Run an instance of the UCSC Genome Browser webserver"
	#:start (make-forkexec-constructor
		  '("/usr/bin/sudo" "/home/shepherd/run_genome_browser.sh")
		  #:log-file "/home/shepherd/logs/genome-browser.log")
	#:stop (make-kill-destructor)
	#:respawn? #t))
(register-services genome-browser)

(start genome-browser)

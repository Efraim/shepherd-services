(define power
  (make <service>
	#:provides '(power)
	#:docstring "Run the BXD Power Calculator app"
	#:start (make-forkexec-constructor
		  ;(system* "/var/guix/profiles/per-user/shepherd/current-guix/bin/guix" "environment" "--ad-hoc" "rn6-assembly-error-app" "--" "/bin/sh -c 'R_LIBS_USER=$GUIX_ENVIRONMENT/site-library/ rn6-assembly-error-app'")
		  '("/home/shepherd/run_power.sh")
		  #:log-file "/home/shepherd/logs/power.log")
	#:stop (make-kill-destructor)
	#:respawn? #t))
(register-services power)

(start power)

(define pluto
  (make <service>
	#:provides '(pluto)
	#:docstring "Run a pluto web server"
	#:start (make-forkexec-constructor
		  '("/home/shepherd/run_pluto.sh")
		  #:log-file "/home/shepherd/logs/pluto.log")
	#:stop (make-kill-destructor)
	#:respawn? #t))
(register-services pluto)

(start pluto)

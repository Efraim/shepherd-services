(define bnw
  (make <service>
	#:provides '(bnw)
	#:docstring "Run the Beyesian Network Webserver"
	#:start (make-forkexec-constructor
		  '("/usr/bin/sudo" "/home/shepherd/run_bnw.sh")
		  #:log-file "/home/shepherd/logs/bnw.log")
	#:stop (make-kill-destructor)
	#:respawn? #t))
(register-services bnw)

(start bnw)

#!/bin/sh
/home/shepherd/guix-profiles/singlecell/bin/guix download https://bioconductor.org/packages/3.11/bioc/src/contrib/multtest_2.44.0.tar.gz
/home/shepherd/guix-profiles/singlecell/bin/guix environment --ad-hoc singlecellrshiny -- sh -c 'R_LIBS_USER=$GUIX_ENVIRONMENT/site-library/ singlecellrshiny'

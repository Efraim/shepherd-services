#!/bin/sh
export EDIRECT_PUBMED_MASTER=/export2/PubMed
export PERL_LWP_SSL_CA_FILE=/etc/ssl/certs/ca-certificates.crt
export NLTK_DATA=/export2/PubMed/nltk_data
unset PATH
$(/home/shepherd/guix-profiles/genecup/bin/guix build edirect)/bin/archive-pubmed

(list
  (channel
    (name 'gn-bioinformatics)
    (url "https://gitlab.com/genenetwork/guix-bioinformatics")
    (commit
      "1e0957387ebf45e40f6f19808a105c2f03c0987f"))
  (channel
    (name 'guix-past)
    (url "https://gitlab.inria.fr/guix-hpc/guix-past")
    (commit
      "99c22ccf19915b4c7982037130571dcf1533cc23")
    (introduction
      (make-channel-introduction
        "0c119db2ea86a389769f4d2b9c6f5c41c027e336"
        (openpgp-fingerprint
          "3CE4 6455 8A84 FDC6 9DB4  0CFB 090B 1199 3D9A EBB5"))))
  (channel
    (name 'guix)
    (url "https://git.savannah.gnu.org/git/guix.git")
    (commit
      "9a299d4d9ee3e1ab3dc89aa89f25db7524a6f6bd")
    (introduction
      (make-channel-introduction
        "9edb3f66fd807b096b48283debdcddccfea34bad"
        (openpgp-fingerprint
          "BBB0 2DDF 2CEA F6A8 0D1D  E643 A2A0 6DF2 A33A 54FA")))))

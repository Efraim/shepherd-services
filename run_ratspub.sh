#!/bin/sh
cd $(/home/shepherd/guix-profiles/ratspub/bin/guix build -L /home/shepherd/guix-past/modules -L /home/shepherd/guix-bioinformatics ratspub-with-tensorflow-native)
/home/shepherd/guix-profiles/ratspub/bin/guix environment -L /home/shepherd/guix-past/modules -L /home/shepherd/guix-bioinformatics --ad-hoc ratspub-with-tensorflow-native -- ./server.py

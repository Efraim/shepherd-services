#!/bin/sh
# This version for the bnw guix profile which includes guix-bioinformatics, using bnw-channels.scm.
$(/home/shepherd/guix-profiles/bnw/bin/guix system container /home/shepherd/guix-bioinformatics/gn/services/bnw-container.scm --share=/home/shepherd/logs/bnw-server=/var/log --network)

#$(/home/shepherd/guix-profiles/bnw/bin/guix system container -L /home/shepherd/guix-past/modules -L /home/shepherd/guix-bioinformatics /home/shepherd/guix-bioinformatics/gn/services/bnw-container.scm --share=/home/shepherd/logs/bnw-server=/var/log --network)

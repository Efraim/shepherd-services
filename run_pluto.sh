#!/bin/sh

/home/shepherd/guix-profiles/pluto/bin/guix build --no-grafts --cores=12 julia-visuals
/home/shepherd/guix-profiles/pluto/bin/guix shell --container --network --pure julia-visuals nss-certs julia --expose=/etc/ssl --share=/home/shepherd/.julia=/home/shepherd/.julia -- runsliderserver

# Path found in the manifest file in the guix-profile
#$(/home/shepherd/guix-profiles/pluto/bin/guix system container /gnu/store/7zhrc956fl9v4x0fvaav5w8fj1ch3rpa-gn-bioinformatics/share/guile/site/3.0/gn/services/pluto.scm --network)

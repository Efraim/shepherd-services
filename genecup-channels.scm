(list
  (channel
    (name 'gn-bioinformatics)
    (url "https://gitlab.com/genenetwork/guix-bioinformatics")
    (commit
      "8274f0d4f5ba8b9b8976dd21057c7d04d09a2e91"))
  (channel
    (name 'guix-past)
    (url "https://gitlab.inria.fr/guix-hpc/guix-past")
    (commit
      "3b663890f53510fa268ae606dd62ca8a2afc5a1a")
    (introduction
      (make-channel-introduction
        "0c119db2ea86a389769f4d2b9c6f5c41c027e336"
        (openpgp-fingerprint
          "3CE4 6455 8A84 FDC6 9DB4  0CFB 090B 1199 3D9A EBB5"))))
  (channel
    (name 'guix)
    (url "https://git.savannah.gnu.org/git/guix.git")
    (commit
      "ecb2603046dfc745a84314ba5921e082a3a68e22")
    (introduction
      (make-channel-introduction
        "9edb3f66fd807b096b48283debdcddccfea34bad"
        (openpgp-fingerprint
          "BBB0 2DDF 2CEA F6A8 0D1D  E643 A2A0 6DF2 A33A 54FA")))))

#!/bin/sh

export EDIRECT_PUBMED_MASTER=/export2/PubMed
export PERL_LWP_SSL_CA_FILE=/etc/ssl/certs/ca-certificates.crt
export TMPDIR=/export/ratspub/tmp
export NLTK_DATA=/export2/PubMed/nltk_data

# This version for the genecup guix profile which includes guix-bioinformatics, using genecup-channels.scm.
/home/shepherd/guix-profiles/genecup/bin/guix environment --no-grafts genecup-with-tensorflow-native -- /home/shepherd/guix-profiles/genecup/bin/guix build --no-grafts genecup-with-tensorflow-native --cores=4
cd $(/home/shepherd/guix-profiles/genecup/bin/guix build genecup-with-tensorflow-native)
/home/shepherd/guix-profiles/genecup/bin/guix environment --ad-hoc genecup-with-tensorflow-native -- ./server.py

#cd $(/home/shepherd/guix-profiles/genecup/bin/guix build -L /home/shepherd/guix-past/modules -L /home/shepherd/guix-bioinformatics genecup-with-tensorflow-native)
#/home/shepherd/guix-profiles/genecup/bin/guix environment -L /home/shepherd/guix-past/modules -L /home/shepherd/guix-bioinformatics --ad-hoc genecup-with-tensorflow-native -- ./server.py

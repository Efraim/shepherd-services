(list
  (channel
    (name 'gn-bioinformatics)
    (url "https://gitlab.com/genenetwork/guix-bioinformatics")
    (commit
     "a81fa9577474254cbf99105ac4ec9b10d7ccf0dd"))
  (channel
    (name 'guix)
    (url "https://git.savannah.gnu.org/git/guix.git")
    (commit
     "5e5bcba29576698fe50bcf9cc8e4de2576583d2d")
    (introduction
      (make-channel-introduction
	"9edb3f66fd807b096b48283debdcddccfea34bad"
	(openpgp-fingerprint
	  "BBB0 2DDF 2CEA F6A8 0D1D  E643 A2A0 6DF2 A33A 54FA")))))

(list
  (channel
    (name 'gn-bioinformatics)
    (url "https://gitlab.com/genenetwork/guix-bioinformatics")
    (commit
      "0ee8053e002a5b19645c6e34ea3c90d5a65596c7"))
  (channel
    (name 'guix-past)
    (url "https://gitlab.inria.fr/guix-hpc/guix-past")
    (commit
      "f4770290109789a5409f98b06ce3c848c43be34d")
    (introduction
      (make-channel-introduction
        "0c119db2ea86a389769f4d2b9c6f5c41c027e336"
        (openpgp-fingerprint
          "3CE4 6455 8A84 FDC6 9DB4  0CFB 090B 1199 3D9A EBB5"))))
  (channel
    (name 'guix)
    (url "https://git.savannah.gnu.org/git/guix.git")
    (commit
      "81b31ca8e1bb4c93e8910f1d3e2e0cf3e8435286")
    (introduction
      (make-channel-introduction
        "9edb3f66fd807b096b48283debdcddccfea34bad"
        (openpgp-fingerprint
          "BBB0 2DDF 2CEA F6A8 0D1D  E643 A2A0 6DF2 A33A 54FA")))))

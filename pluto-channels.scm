(list
  (channel
    (name 'gn-bioinformatics)
    (url "https://git.genenetwork.org/guix-bioinformatics/")
    (commit
      "f1b32a21b7c45e3b974ecb543d845c2246339367"))
  (channel
    (name 'guix-past)
    (url "https://gitlab.inria.fr/guix-hpc/guix-past")
    (commit
      "1e25b23faa6b1716deaf7e1782becb5da6855942")
    (introduction
      (make-channel-introduction
        "0c119db2ea86a389769f4d2b9c6f5c41c027e336"
        (openpgp-fingerprint
          "3CE4 6455 8A84 FDC6 9DB4  0CFB 090B 1199 3D9A EBB5"))))
  (channel
    (name 'guix)
    (url "https://git.savannah.gnu.org/git/guix.git")
    (commit
      "15c5f1a2c20b21de0f19f42db1ccab4c42117ebb")
    (introduction
      (make-channel-introduction
        "9edb3f66fd807b096b48283debdcddccfea34bad"
        (openpgp-fingerprint
          "BBB0 2DDF 2CEA F6A8 0D1D  E643 A2A0 6DF2 A33A 54FA")))))

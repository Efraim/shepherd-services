#!/bin/sh
/bin/su -l ipfs -c 'IPFS_PATH=/export/ipfs $(/home/shepherd/guix-profiles/ipfs/bin/guix build go-ipfs)/bin/ipfs daemon --enable-gc'

#!/bin/sh
/home/shepherd/guix-profiles/genome-browser/bin/guix build --no-grafts ucsc-genome-browser
$(/home/shepherd/guix-profiles/genome-browser/bin/guix system container /home/shepherd/guix-bioinformatics/gn/services/genome-browser.scm --network --share=/export4/efraimf/UCSC_Genome/gbdb=/gbdb --share=/export4/efraimf/UCSC_Genome/var-lib-mysql=/var/lib/mysql --share=/export4/efraimf/UCSC_Genome/var-cache-genome=/var/cache/genome --share=/export4/efraimf/UCSC_Genome/var-cache-genome=/var/www/html/trash)

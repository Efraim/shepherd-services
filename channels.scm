(cons*
  (channel
    (name 'gn-bioinformatics)
    (url "https://gitlab.com/genenetwork/guix-bioinformatics")
    (branch "master"))
  %default-channels)
